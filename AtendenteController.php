<?php

/*
  Copyright (c) <2017> <Davi Felipe Menezes de Sosua>

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

/**
 * Description of AtendenteController
 *
 * @author davi felipe <davifelipemsousa@gmail.com>
 */
class AtendenteController {
    
    public function indexAction(){
        
        $this->_pageRender = false;
        $this->_pagePartialRender = false;
        
        try {
       
            $this->limpaSessao();
            
            $this->enviaConversa();
            
            $dadosAtendente = array('nome_atend'=>Config::getParam("nome-atend",$this->_params['hash']),
                                    'boas_vindas_atend'=>Config::getParam("boas-vindas-atend",$this->_params['hash']),
                                    'resposta_padrao_atend'=>Config::getParam("resposta-padrao-atend",$this->_params['hash']),
                                    'resposta_contato_ok'=>Config::getParam("resposta-contato-ok",$this->_params['hash']),
                                    );
            
            $json = array('msg'=>array('txt'=>'ok','cod'=>0),
                          'dados_atendente'=>$dadosAtendente);
            
        } catch (Exception $e) {
            $json = array('msg'=>array('txt'=>$e->getMessage(),'cod'=>1),
                          'dados_atendente'=>array());
        }
        
//        header('Access-Control-Allow-Origin: *');
//        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept"); 

        echo json_encode($json);
        
    }
    
    public function finalizaAction(){
        
         $this->_pageRender = false;
        $this->_pagePartialRender = false;
        
        try {
            
             if($_SESSION['chave']){
                 $this->enviaConversa($_SESSION['chave']);
                 $this->limpaSessao();
             }
            
             $json = array('msg'=>array('txt'=>'ok','cod'=>0));
            
        } catch (Exception $e) {
            $json = array('msg'=>array('txt'=>$e->getMessage(),'cod'=>1));
        }
        
//        header('Access-Control-Allow-Origin: *');
//        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept"); 
        echo json_encode($json);
        
    }
    
    private function enviaConversa($dataHora = ''){
        
            $historico = new Atendhistorico();
             $siteUsuario= new Siteusuario();
            //envia as conversas que não foi enviada
            $where[] = "enviado = 'N'";
            
            $dataHoje = date("Y-m-d");
            
            $horaIs = date("i:s");
            
            $hora = date("H");
            
            if(!empty($dataHora)){
                $where[] = "data_hora = '{$dataHora}'";
            }else{
                
                if((int)$hora == 0){
                    $umaHoraAtras = 23;
                }else{
                    $umaHoraAtras = $hora-1;
                }
                
                
                $where[] = "data_hora <= '{$dataHoje} {$umaHoraAtras}:{$horaIs}'";
            }
            
            $naoEnviada = $historico->fetchRow($where);
            
            
            if(trim($this->_params['hash']) != ''){
               
                $rUsuario = $siteUsuario->fetchRow("id_hash = '{$this->_params['hash']}'");

                $emailDestino = $rUsuario['email'];
                
            }
            
            if($naoEnviada['id_usuario'] > 0){
               
                $rUsuario = $siteUsuario->fetchRow("id_usuario = {$naoEnviada['id_usuario']}");
                $emailDestino = $rUsuario['email'];
            }
            
            if(empty($emailDestino)){
                $emailDestino = SMVC_Start::getSmtpEmailAdmin();
            }
            
            $nomeConfigHash = Config::getParam("nome-atend",$this->_params['hash']);
            $nomeConfigId = Config::getParam("nome-atend",$rUsuario['id_hash']);
            
            $nomeAtend = ( $nomeConfigId != '' ? $nomeConfigId : $nomeConfigHash);
            
            $emailDebug = false;
            
            $debug = ($emailDebug ? serialize($where) : "");
            
            if($naoEnviada['id_historico'] > 0
            && !substr_count($_SERVER['SERVER_NAME'], '.local')){
                
                 LP_Util::sendGenerico($emailDestino,
                                    SMVC_Start::getSmtpEmailFrom(),
                                  'admin',
                                   LP_Util::converteEspecialCharsParaHtml($naoEnviada['conversa_html']).$debug,
                                  'Contato via site (Atendente virtual)',
                                   $nomeAtend);
                 
                 $historico->update("id_historico = '{$naoEnviada['id_historico']}'",
                                    array('enviado'=>'S'));
            }
        
    }
    
    public function testeAction(){
     
        $this->_pageRender = false;
        $this->_pagePartialRender = false;
        
        if(!substr_count($_SERVER['SERVER_NAME'], '.local')){
            die('');
        }
        
        
     }
     
    public function respostaAction(){
     
        $this->_pageRender = false;
        $this->_pagePartialRender = false;
        
        try {
            
            parse_str($this->_params['dadosForm'],$dadosForm);
            
            if(!empty($dadosForm)){
                foreach ($dadosForm as $campo => $valor) {
                    if(empty($this->_params[$campo])){
                        $this->_params[$campo] = $valor;
                    }
                }
            }
            
            //nome texto
            
            $ip = $_SERVER['REMOTE_ADDR'];
            
            if(!$_SESSION['chave']){
                $_SESSION['chave'] = date('Y-m-d H:i:s');
            }
            
            if(!empty($this->_params['nome'])){
                foreach ($this->_params['nome'] as $key => $nome) {
                    
                    $dados[] = array('nome'=>$nome,
                                     'texto'=>$this->_params['texto'][$key]
                                   );
                }
                
               
                
                $resposta = $this->getResposta();
                
                $dadosAtendente = array('nome_atend'=>Config::getParam("nome-atend",$this->_params['hash']));
                
                $dados[] = array('nome'=>$dadosAtendente['nome_atend'],'texto'=>$resposta);
                
                $historico = new Atendhistorico();
                
                $detalhes = $this->getResponse("http://ip-api.com/json/{$ip}",$_POST);
                
                $dadosDet = json_decode($detalhes);
                
                $detArray = array();
                
                if(isset($dadosDet->city)){
                    $detArray['city'] = $dadosDet->city;
                }
                
                if(isset($dadosDet->country)){
                    $detArray['country'] = $dadosDet->country;
                }
                
                if(isset($dadosDet->isp)){
                    $detArray['isp'] = $dadosDet->isp;
                }
                
                if(isset($dadosDet->lat)){
                    $detArray['lat'] = $dadosDet->lat;
                }
                
                if(isset($dadosDet->lon)){
                    $detArray['lon'] = $dadosDet->lon;
                }
                
                if(isset($dadosDet->org)){
                    $detArray['org'] = $dadosDet->org;
                }
                
                if(isset($dadosDet->region)){
                    $detArray['region'] = $dadosDet->region;
                }
                
                if(isset($dadosDet->regionName)){
                    $detArray['regionName'] = $dadosDet->regionName;
                }
                
                if(isset($dadosDet->timezone)){
                    $detArray['timezone'] = $dadosDet->timezone;
                }
                
                
                $detArray['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
                
                
                $rHistorico = $historico->fetchRow(array("data_hora = '{$_SESSION['chave']}'",
                                                         "ip = '{$ip}'",
                                                          "detalhes = '".  serialize($detArray)."'"));
                
                $dadosBanco = array('ip'=>$ip,
                                    'data_hora'=>$_SESSION['chave'],
                                    'conversa'=>  serialize($dados),
                                    'detalhes'=>  serialize($detArray));                                         
                
                if($this->_params['hash'] != ''){
                    $siteUsuario= new Siteusuario();
                    $rUsuario = $siteUsuario->fetchRow("id_hash = '{$this->_params['hash']}'");

                    if($rUsuario['id_usuario'] >0){
                        $dadosBanco["id_usuario"] = $rUsuario['id_usuario'];
                    }
                }
                
                if($rHistorico['id_historico']>0){
                     
                    $historico->update("id_historico = {$rHistorico['id_historico']}",$dadosBanco);
                }else if($_SESSION['chave'] != ''){
                    
                    $historico->insert($dadosBanco);
                }                                        
                
            }
            
            
            
            $json = array('msg'=>array('txt'=>'ok','cod'=>0),
                          'resposta'=>$resposta,
                          'data_hora'=>$_SESSION['chave'],
                           'dados_atendente'=>$dadosAtendente);
            
        } catch (Exception $e) {
            $json = array('msg'=>array('txt'=>$e->getMessage(),'cod'=>1),
                          'resposta'=>'','dados_atendente'=>array());
        }
        
        header('Access-Control-Allow-Origin: *');   
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept"); 

        echo json_encode($json);
    }
    
    private function getResponse($url,$postdata){
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 20);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = trim(curl_exec($curl));
        curl_close($curl);

        return $result;
    }
    
    public function limpaAction(){
        
        $this->_pageRender = false;
        $this->_pagePartialRender = false;
        
        if(substr_count($_SERVER['SERVER_NAME'],".local")){
            $this->limpaSessao();
            echo 'limpo';
        }
        
        
        
    }
    
    public function limpaSessao($limpaChave = true){
        
            if($limpaChave){
                unset($_SESSION['chave']);
            }
                        
            unset($_SESSION['telefone_ok']);
            unset($_SESSION['dd_ok']);
            unset($_SESSION['nome_ok']);
            unset($_SESSION['deu_resposta_correta']);
            unset($_SESSION['deu_resposta_padrao']);
            unset($_SESSION['pediu_telefone']);
            unset($_SESSION['pegou_contato']);
        
    }
    
    private function validaPalavra($str){
        
        
        $palavras = explode(' ', $str);
        
        $vogais = array('a','e','i','o','u');
        
        $valido = true;
        
        if(!empty($palavras)){
            foreach ($palavras as $key=>$palavra) {
                $tamanho = strlen($palavra);
                $qtdConsoantes = 0;
                for ($i = 0; $i < $tamanho; $i++) {
                    $char = substr($palavra, $i, 1);
                   
                    if(!in_array($char, $vogais)){
                       $qtdConsoantes++;
                       $arraryQtdConsoantes[$key] = $qtdConsoantes;
                       $arrayConsoantes[$key].=$char;
                    }else{
                        $qtdConsoantes = 0;
                    }
                }
            }
            
            if(!empty($arraryQtdConsoantes)){
                $maior = 0;
                foreach ($arraryQtdConsoantes as $keyM=> $valor) {
                    
                    if($maior == 0 || $valor > $maior){
                        $maior = $valor;
                        $keyValorMaior = $keyM;
                    }
                    
                }
            }
            
            
            $maiorNumeroDeConsoantesJuntas = $arrayConsoantes[$keyValorMaior];
            
          
            //se tiver 3 ou mais consoantes juntas
            if($maior >= 3 && !is_numeric($maiorNumeroDeConsoantesJuntas)){
                $execoes = array('kkk',//risada
                                 'nst','nstr',//instrução, instrumento,
                                 'mpr','str',// empresa, maestro..
                                 'mbr',// setembro
                                 'ltr',//filtro
                                 'cpf',//cpf
                                 'cnpj',//cnpj
                                 'spl',//displicente
                                 'ngu', //lingua..
                                 'blz', //beleza..
                                 'ntr', //centro..
                                 'vcs',
                                 'lly',
                                 'tbm',//ambem
                                 'ndr',//andre
                                 'ddd',//DDD
                                 'DDD',//DDD
                                 'qto',//quarto
                                 'qts',//quartos
                                 'sms',
                                 '.',
                                 '!',
                                 '?',
                                 ')',
                                 '(',
                                 '-',
                                 '/',
                                 '+',
                                 '=',
                                 ',',
                                 '0','1','2','3','4','5','6','7','8','9',
                                );
                    $valido = false;
                    foreach ($execoes as $termoEx) {
                        $termoEx = LP_Util::removeCaracteresEspeciais(strtolower($termoEx), " ");
                       
                        if(substr_count(strtolower($maiorNumeroDeConsoantesJuntas), $termoEx)){
                            $valido = true;
                        }
                    }
            }
            
        }
        
        return $valido;
    }
    
    public function getResposta(){
        
        $caracteresTelefone = array(")","(","-");
        
        $resposta = new Atendresposta();
        
        $palavrasNegativaModel = new Palavranegativa();
        
        $pergunta = strtolower(LP_Util::removeCaracteresEspeciais($this->_params['mensagem'], " "));
        
        $perguntaDigitada = strtolower($pergunta);
        
        if($this->_params['hash'] != ''){
            $siteUsuario= new Siteusuario();
            $rUsuario = $siteUsuario->fetchRow("id_hash = '{$this->_params['hash']}'");
            
            if($rUsuario['id_usuario'] >0){
                $whereAbs[] = "id_usuario = {$rUsuario['id_usuario']}";
            }
        }else{
            $whereAbs[] = "id_usuario = 0";
        }
        
         $whereAbs[] =  "palavras_chave LIKE '\"%'";
        
        $respostasAbsolutas = $resposta->fetchAll($whereAbs);
        
        if(!empty($respostasAbsolutas)){
            foreach ($respostasAbsolutas as $key => $rRepspostaAbsoluta) {
                
                $palavraChaveTratada = str_replace('"', '', strtolower($rRepspostaAbsoluta['palavras_chave']));
                
                if(trim($perguntaDigitada) == trim($palavraChaveTratada)){
                    return $rRepspostaAbsoluta['resposta'];
                }
            }
        }
        
        
        
        if($this->_params['hash'] != ''){
            $siteUsuario= new Siteusuario();
            $rUsuario = $siteUsuario->fetchRow("id_hash = '{$this->_params['hash']}'");
            
            if($rUsuario['id_usuario'] >0){
                $whereResposta[] = "id_usuario = {$rUsuario['id_usuario']}";
            }
        }else{
            $whereResposta[] = "id_usuario = 0";
        }
        
        $whereResposta[] = "palavras_chave NOT LIKE '\"%'";
        
        $respostas = $resposta->fetchAll($whereResposta);
        
        $respostaJaPegouContato = "Desculpe, mas ainda não recebi instrução para falar sobre isso. \n Se quiser pode fazer outra pergunta \n Mas retornaremos o contato de qualquer maneira.";
        
        $pegarTelefone = (Config::getParam("pega-telefone",$this->_params['hash']) != 'N');
        
       
                
        
        $numero = str_replace($caracteresTelefone, "", $pergunta);
        
        $naoPegouTelefone = (!$_SESSION['telefone_ok'] || !$_SESSION['dd_ok']);
        $jaPegouTelefone = ($_SESSION['telefone_ok'] && $_SESSION['dd_ok']);
        
        //procura o número na conversa.
        $tamanho = strlen($pergunta);
        for ($i = 0; $i < $tamanho; $i++) {
            $char = substr($pergunta, $i, 1);
            if(is_numeric($char)){
                $numeroColetado.= $char;
            }else{
                $nomeColetado.=$char;
            }
        }
        
        
        $temTelefone = (strlen($numeroColetado) >= 8 && strlen($numeroColetado) <= 11);
        
        if(trim($nomeColetado) != ''){
            $nomeColetado = str_replace($caracteresTelefone, "", $nomeColetado);
                 
        }
        
        if(!is_numeric($numero) 
        && $temTelefone){
            
            $numero = $numeroColetado;
            
                if(strlen($nomeColetado)> 2){
                    $_SESSION['nome_ok'] = true;
                }
        }
        
        if(strlen($nomeColetado)> 2 && $temTelefone){
           
            $_SESSION['nome_ok'] = true;
        }
        
        //se for pergunta numérica, tenta achar o DDD
        if(is_numeric($numeroColetado) && $naoPegouTelefone){
            $primeiro = substr($numero, 0, 1);
            
            if($primeiro == 0){
                
                $ddd = substr($numero, 0, 3);
                $ref = 3;
            }else{
                
                $ddd = substr($numero, 0, 2);
                $ref = 2;
            }
            
             $telefone =  substr($numeroColetado, $ref, 8);
         
             if(strlen($numero) >= 8 && strlen($numero) <= 9){
                
                 $_SESSION['telefone_ok'] = true;
               
             }
             
             if(strlen($telefone) >= 8 && strlen($telefone) <= 9){
                 $_SESSION['telefone_ok'] = true;
                 $_SESSION['dd_ok'] = true;
               
                 
             }
             
             if(strlen($numero) >= 2 && strlen($numero) <= 3){
                 $_SESSION['dd_ok'] = true;
               
             }
             
        }else if($jaPegouTelefone && strlen($pergunta) >= 2){
                
                 $_SESSION['nome_ok'] = true;
        }
        
        $naoPegouTelefone = (!$_SESSION['telefone_ok'] || !$_SESSION['dd_ok']);
        $jaPegouTelefone = ($_SESSION['telefone_ok'] && $_SESSION['dd_ok']);
            
        //primeiro procura resposta mais adequada
        if(!empty($respostas)){
            foreach ($respostas as $key => $rResposta) {
                
                $aOrAgrupado = explode(",",$rResposta['palavras_chave']);
                
                  
                if(!empty($aOrAgrupado)){
                    $refKey = 0;
                    foreach ($aOrAgrupado as $key1 => $termoAg) {

                        $arrayChavesChar = array("[","]");

                        if($key1 == 0 || $key1 == (count($aOrAgrupado)-1)){
                           $aOrAgrupado[$key1] = str_replace($arrayChavesChar, "", $termoAg);
                           $termoAg = str_replace($arrayChavesChar, "", $termoAg);
                        }

                        $fistChar = substr($termoAg, 0, 1);
                        $tamanhoChar = strlen($termoAg);
                        $lastChar = substr($termoAg, ($tamanhoChar-1), 1);

                        if($fistChar == "["){
                            $refKey++; 
                        }

                        $aOrFinal[$refKey][] = str_replace($arrayChavesChar, "", $termoAg);

                        if($lastChar == "]"){
                            $refKey++; 
                        }

                    }
                }
                    
                $agrupadoEmOr = (count($aOrFinal) > 1);
                
                $arrayKeyWords = explode(",", $rResposta['palavras_chave']);
                
                if(!empty($arrayKeyWords)){
                    foreach ($arrayKeyWords as $key2 => $termo) {

                        $tamanho = strlen($termo);

                        $abre = (substr($termo, 0, 1) == '[');
                        $fecha = (substr($termo, ($tamanho-1), 1) == ']');

                        if($abre || $abriu){
                            $abriu = true;
                            $arrayOR[] = trim(str_replace('[', '', str_replace(']', '', $termo)));
                        }else{
                            $arrayAnd[] = trim($termo);
                        }

                        if($fecha){
                            $abriu = false;
                        }
                    }

                }
                
                $achouOr = false;
                
                
                if(!empty($arrayOR)){
                     foreach ($arrayOR as $key2 => $termoOr) {
                         if(substr_count(strtolower($pergunta), strtolower($termoOr))){
                             $achouOr = true;
                             $respostaEncontradas[$rResposta['id_resposta']]['termos'][$termoOr]=$termoOr;
                         }
                     }
                }else{
                    $achouOr = true;
                }
                
                
                $achouAnd = true;

                if(!empty($arrayAnd)){
                     foreach ($arrayAnd as $key2 => $termoAnd) {
                         if(!substr_count(strtolower($pergunta), strtolower($termoAnd))){
                             $achouAnd = false;
                         }else{
                             $respostaEncontradas[$rResposta['id_resposta']]['termos'][$termoAnd]=$termoAnd;
                         }
                     }
                }
                
               
                
                if($agrupadoEmOr){
                    
                    foreach ($aOrFinal as $key2 => $itemOr) {
                        
                        $achouOr = false;
                        
                         if(!empty($itemOr)){
                              foreach ($itemOr as $key3 => $termoOr) {
                                  if(substr_count($pergunta, $termoOr)){
                                      $achouOr = true;
                                      $respostaEncontradas[$rResposta['id_resposta']]['termos'][$termoOr]=$termoOr;
                                  }
                              }
                         }else{
                            $achouOr = true;
                        }

                         $resultadoOrAgrupado[$key2] = $achouOr;

                    }
                    
                    
                    
                    $achouAnd = true;

                     if(!empty($resultadoOrAgrupado)){
                          foreach ($resultadoOrAgrupado as $key2 => $result) {
                              if(!$result){
                                  $achouAnd = false;
                              }
                          }
                     }
                     
                     
                     if($achouAnd){
                          
                         //return $rResposta['resposta'];
                         $respostaEncontradas[$rResposta['id_resposta']]['resposta'] = $rResposta['resposta'];
                     }

                }
                
                 if($achouOr && $achouAnd){
                     
                    //return $rResposta['resposta'];
                     $respostaEncontradas[$rResposta['id_resposta']]['resposta'] = $rResposta['resposta'];
                 }
              
                unset($achouOr);
                unset($achouAnd);
                unset($arrayAnd);
                unset($resultadoOrAgrupado);
                unset($arrayOR);
                unset($aOrFinal);
                unset($aOrAgrupado);
            } 
            
            //Remove respostas com palavras negativas
            if(!empty($respostaEncontradas)){
                foreach ($respostaEncontradas as $idResposta => $rOcorencia) {
                    $palavrasNegativa = $palavrasNegativaModel->fetchAll("id_resposta = {$idResposta}");
                    
                    $temPalavraNegativa = false;
                    
                    if(!empty($palavrasNegativa)){
                        foreach ($palavrasNegativa as $rPalavraNegativa){
                              if(substr_count($perguntaDigitada, $rPalavraNegativa['palavra'])){
                                  $temPalavraNegativa = true;
                              }  
                        }
                    }
                    
                    if($temPalavraNegativa){
                        unset($respostaEncontradas[$idResposta]);
                    }
                }
            
            }     
            
            if(!empty($respostaEncontradas)){
                
                
                $maiorQtdTermo = 0;
                foreach ($respostaEncontradas as $idResposta => $rOcorencia) {
                    
                    
                    $qtdTermosEncontrados = count($rOcorencia['termos']);
                    
                    $achouMaior = ($maiorQtdTermo == 0 || $qtdTermosEncontrados > $maiorQtdTermo);
                    
                    if($achouMaior && trim($rOcorencia['resposta'])!= ''){
                        $maiorQtdTermo = $qtdTermosEncontrados;
                        $rRespostaMaiorQtdTermo = $rOcorencia;
                    }
                }
                
                if(trim($rRespostaMaiorQtdTermo['resposta']) != ''){
                    return $rRespostaMaiorQtdTermo['resposta'];
                }
                
            }
            
        }
        
        
        //valida palavra em português
        if(!$this->validaPalavra($pergunta)){
            return "{$pergunta}? \n Não entendi. \n Talvez você tenha digitado alguma palavra que não existe.";
        }
        
        if(!$achouTodasNaPergunta
        && !$_SESSION['deu_resposta_padrao']
        && !$_SESSION['telefone_ok']){
            
            $_SESSION['deu_resposta_padrao'] = true;
             if($_SESSION['pegou_contato']){
               
                return $respostaJaPegouContato;
            }
            return Config::getParam("resposta-padrao-atend",$this->_params['hash']);
        }
        
        //telefone sem DDD
        if($_SESSION['telefone_ok'] && !$_SESSION['dd_ok'] && $pegarTelefone){
            return "Preciso que me informe o DDD.";
        }
        
        //Nem telefone nem DD
        if(!$jaPegouTelefone && $pegarTelefone){
            $_SESSION['pediu_telefone']++;
            if($_SESSION['pediu_telefone'] >=2){
                $_SESSION['pediu_telefone'] = 0;
                
                
                
                if($_SESSION['pegou_contato']){
                    return $respostaJaPegouContato;
                }
                return "Estou tentando lhe ajudar.. \n Informe um número válido ou então faça outra pergunta.";                
            }
            
            if($_SESSION['pegou_contato']){
                return $respostaJaPegouContato;
            }
            return "Por favor informe seu telefone.";
        }
        
        //falta só o nome
        if(!$_SESSION['nome_ok']&& $pegarTelefone){
            return "Ok, já anotei seu telefone. \n agora por favor informe seu nome.";
        }
        
        
        if($jaPegouTelefone && $_SESSION['nome_ok'] && $pegarTelefone){
            unset($_SESSION['deu_resposta_padrao']);
           
            $this->limpaSessao(false);
            $_SESSION['pegou_contato'] = true;
            
            return Config::getParam("resposta-contato-ok",$this->_params['hash']);
        }
        
        return Config::getParam("resposta-padrao-atend",$this->_params['hash']);

        
    }
    
    public function paginaAction(){
        $this->_pageRender = false;
        
        $this->toView('corAtend', Config::getParam('cor-atend', $this->_params['hash']));
        $this->toView('fotoAtend', Config::getParam('foto-atend', $this->_params['hash']));
        $this->toView('logoAtend', Config::getParam('logo-atend', $this->_params['hash']));
        $this->toView('fotoJanela', Config::getParam('janela-atend', $this->_params['hash']));
        $this->toView('janelaPadrao', Config::getParam('janela-padrao', $this->_params['hash']));
        $this->toView('tituloJanela', Config::getParam('titulo-janela', $this->_params['hash']));
        
        $backlink = LP_Util::getBacklink();
        
        $this->toView('backlink', $backlink);
        
        $siteUsuario = new Siteusuario();
        if($this->_params['hash'] != ''){
            $rUsuario = $siteUsuario->fetchRow("id_hash = '{$this->_params['hash']}'");
            
            $dataHoje = date('Y-m-d');
            
             $dataLimite = LP_Util::dateFormatToEn($rUsuario['data_limite']);
            
             $acessoExpirado = ($dataLimite < $dataHoje);
             
             if($acessoExpirado){
                 $this->_pagePartialRender = false;
             }
        }
            
        
        $this->toView('hash', $this->_params['hash']);
        
    }
    
    public function popup2Action(){
        $this->_pageRender = false;
        
        $this->toView('corAtend', Config::getParam('cor-atend', $this->_params['hash']));
        $this->toView('fotoAtend', Config::getParam('foto-atend', $this->_params['hash']));
        $this->toView('logoAtend', Config::getParam('logo-atend', $this->_params['hash']));
        $this->toView('fotoJanela', Config::getParam('janela-atend', $this->_params['hash']));
        $this->toView('janelaPadrao', Config::getParam('janela-padrao', $this->_params['hash']));
        $this->toView('tituloJanela', Config::getParam('titulo-janela', $this->_params['hash']));
        
        $backlink = LP_Util::getBacklink();
        
        $this->toView('backlink', $backlink);
        
        $siteUsuario = new Siteusuario();
        if($this->_params['hash'] != ''){
            $rUsuario = $siteUsuario->fetchRow("id_hash = '{$this->_params['hash']}'");
            
            $dataHoje = date('Y-m-d');
            
             $dataLimite = LP_Util::dateFormatToEn($rUsuario['data_limite']);
            
             $acessoExpirado = ($dataLimite < $dataHoje);
             
             if($acessoExpirado){
                 $this->_pagePartialRender = false;
             }
        }
            
        
        $this->toView('hash', $this->_params['hash']);
        
    }
    
    public function popupAction(){
        $this->_pageRender = false;
        
        $this->toView('corAtend', Config::getParam('cor-atend', $this->_params['hash']));
        $this->toView('fotoAtend', Config::getParam('foto-atend', $this->_params['hash']));
        $this->toView('logoAtend', Config::getParam('logo-atend', $this->_params['hash']));
        $this->toView('fotoJanela', Config::getParam('janela-atend', $this->_params['hash']));
        $this->toView('janelaPadrao', Config::getParam('janela-padrao', $this->_params['hash']));
        $this->toView('tituloJanela', Config::getParam('titulo-janela', $this->_params['hash']));
        
        $backlink = LP_Util::getBacklink();
        
        $this->toView('backlink', $backlink);
        
        $siteUsuario = new Siteusuario();
        if($this->_params['hash'] != ''){
            $rUsuario = $siteUsuario->fetchRow("id_hash = '{$this->_params['hash']}'");
            
            $dataHoje = date('Y-m-d');
            
             $dataLimite = LP_Util::dateFormatToEn($rUsuario['data_limite']);
            
             $acessoExpirado = ($dataLimite < $dataHoje);
             
             if($acessoExpirado){
                 $this->_pagePartialRender = false;
             }
        }
            
        
        $this->toView('hash', $this->_params['hash']);
        
    }
    
    public function getvaluesAction(){
        
        $this->_pageRender = false;
        $this->_pagePartialRender = false;
        
        $corAtend = Config::getParam('cor-atend', $this->_params['hash']);
        $fotoAtend = Config::getParam('foto-atend', $this->_params['hash']);
        $logoAtend = Config::getParam('logo-atend', $this->_params['hash']);
        $fotoJanela = Config::getParam('janela-atend', $this->_params['hash']);
        $janelaPadrao = Config::getParam('janela-padrao', $this->_params['hash']);
        $tituloJanela = Config::getParam('titulo-janela', $this->_params['hash']);
        
        $baseUrl = SMVC_Start::getBaseUrl();
        
     $htmlJanela = "<link rel=\'stylesheet\' href=\'{$baseUrl}/css/atendente-virtual.css\' type=\'text/css\' />\\n\\
                    <script type=\"text/javascript\">\\n\\
                    $(\'document\').ready(function(){\\n\\";
        
        if(!$_SESSION['iniciou_atendente'] && Config::getParam("abre-automatico", $this->_params['hash']) == 'S'): $_SESSION['iniciou_atendente'] = 'S';
        $htmlJanela.= 'iniciaAtendente();';
         endif;
         
       
   $htmlJanela.= '
    });</script>\\n\\
    \\n\\
</script>\\n\\
<style type="text/css">\\n\\
    .bg-atend{\\n\\
        background-image: '.($logoAtend != '' ? "url({$baseUrl}/logo/{$logoAtend})" : 'none').';\\n\\
        background-position: right;\\n\\
        background-repeat: no-repeat;\\n\\
    }\\n\\
    .container-popup-atv #title-bar {\\n\\
        background-color: '.($corAtend != '' ? $corAtend : '#000000').';\\n\\
    }\\n\\
</style>';

        
if($fotoJanela != '' && $janelaPadrao != 'S'):

$htmlJanela.= '\\n\\
<div class="container-popup-atv" style="border: none; background: none;" id="pupup-inicial">\\n\\
    <a href="javascript:abrirConversa();">\\n\\
        <img src="'.$baseUrl.'/janela/'.$fotoJanela.'" />\\n\\
    </a>\\n\\
     <div class="atendente-caregando" style="position: absolute; left:130px; top: 15px; display: none;">\\n\\
            <img src="'.$baseUrl.'/images/carregando.GIF" alt="carregando"/>\\n\\
        </div>\\n\\
</div>';

else:

$htmlJanela.= '\\n\\
<div class="container-popup-atv" style="width: 190px;" id="pupup-inicial">\\n\\
    <div id="cabecalho" class="bg-tema">\\n\\
        <img style="float: left;" src="'.$baseUrl.'/'.($fotoAtend != '' ? "foto_atend/{$fotoAtend}" : 'images/atend.png').'" alt="atendente"/>\\n\\
        <div id="corpo" style="float: left;">\\n\\
            <a href="javascript:abrirConversa();">Precisa de ajuda?</a> <br />\\n\\
            <a href="javascript:abrirConversa();">Clique aqui!</a>\\n\\
        </div>\\n\\
        <div class="atendente-caregando" style="position: absolute; left:130px; top: 15px; display: none;">\\n\\
            <img src="'.$baseUrl.'/images/carregando.GIF" alt="carregando"/>\\n\\
        </div>\\n\\
    </div>\\n\\
</div>';
endif;




 $htmlJanela.= '\\n\\
<div class="container-popup-atv" id="pupup-conversa" style="display: none;">\\n\\
    <div id="inter">\\n\\
        <div id="title-bar">\\n\\
            '.$tituloJanela.'\\n\\
            <div id="contoller" onclick="fecharConversa();">\\n\\
                <img src="'.$baseUrl.'/images/fechar.png" alt="Fechar"/>\\n\\
            </div>\\n\\
        </div>\\n\\
        <div id="cabecalho" class="bg-atend">\\n\\
            <img src="'.$baseUrl.'/'.($fotoAtend != '' ? "foto_atend/{$fotoAtend}" : "images/atend.png").'" alt="atendente"/>\\n\\
        </div>\\n\\
        <div id="corpo">\\n\\
            <div id="conversa">\\n\\
                <ul id="lista-conversa">\\n\\
                </ul>\\n\\
            </div>\\n\\
            <div id="painel">\\n\\
                <input type="checkbox" value="ON" checked="checked" onchange="descerConversa(3000);" id="rolagem-automarica" />\\n\\
                Rolagem autom&aacute;tica\\n\\
            </div>\\n\\
            <div id="msg">\\n\\
                <textarea style="width: 303px;height: 77px;" cols="35" rows="4" id="atend-mensagem" >Digite aqui e pressione enter</textarea>\\n\\
                <a href="http://www.atendentevirtualnoseusite.com" style="font-size: 8px;" target="_blank">\\n\\
                    powered by Atendente Virtual\\n\\
                </a>\\n\\
            </div>\\n\\
            <form style="display: none;" id="form-historico">\\n\\
            </form>\\n\\
        </div>\\n\\
    </div>\\n\\
</div>';
            
          
         echo "htmlJanela = '{$htmlJanela}';";
         
         echo "
             hash = '{$this->_params['hash']}';
         ";
             
         echo "
              baseUrl = '{$baseUrl}';
             ";    
         
         $this->loadJsFile("{$baseUrl}/js/jquery.min1.6.js");
        
         $this->loadJsFile("{$baseUrl}/js/atendente-virtual.js");
         
         echo "var divTag=document.createElement(\"div\");
                divTag.id=\"div1\";
                divTag.innerHTML= htmlJanela;
                document.body.appendChild(divTag);";
        
    }
    
    
    private function loadJsFile($file){
          echo "var js = document.createElement(\"script\");
                js.type = \"text/javascript\";
                js.src = \"{$file}\";
                document.body.appendChild(js);";
    }
    
   
}
